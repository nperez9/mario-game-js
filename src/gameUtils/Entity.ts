import { Vec2 } from './Vectors';
import { SpriteSheet } from './SpriteSheet';

export class Entity {
  public position: Vec2;
  public velocity: Vec2;
  public sprite: SpriteSheet;

  constructor(sprite: SpriteSheet) {
    this.position = new Vec2(0, 0);
    this.velocity = new Vec2(0, 0);
    this.sprite = sprite;
  }

  public draw(context: CanvasRenderingContext2D): void {
    this.sprite.draw('idle', context, this.position.x, this.position.y);
  }
}