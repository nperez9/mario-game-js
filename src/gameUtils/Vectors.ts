/**
 * Generic bidimencional vector
 */
export class Vec2 {
  public x: number;
  public y: number;

  constructor(x: number, y: number) {
    this.set(x, y);
  }

  /**
   * set the position of x and y
   * @param x x position
   * @param y y position
   */
  public set(x: number, y: number): void {
    this.x = x;
    this.y = y;
  }
}