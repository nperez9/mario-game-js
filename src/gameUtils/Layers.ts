import { SpriteSheet } from './SpriteSheet';
import { Entity } from './Entity';
import { IBackground } from 'src/interfaces';

/**
 * Helper to creates the Layer than Compositor class are going to draw
 * TODO: refator this has a factory and made a drawable object for compositor (also refactor Compositor.ts)
 */
export class Layers {
  private static drawBackgrounds(backgrounds: IBackground[], context: CanvasRenderingContext2D, sprites: SpriteSheet) {
    backgrounds.forEach(background => {  
      background.ranges.forEach(([x1, x2, y1, y2]) => {
        for(let x = x1; x < x2; ++x) {
          for(let y = y1; y < y2; ++y) {
            sprites.drawTile(background.tile, context, x, y);
          }
        }
      });
    });
  }

  /**
   * Generates a background game layer
   * @param backgrounds Array with the backgrounds data 
   * @param sprites sprites of the backrounds
   */
  public static createBackground(backgrounds: IBackground[], sprites: SpriteSheet): Function {
    const buffer = document.createElement('canvas') as HTMLCanvasElement;
    buffer.width = 256;
    buffer.height = 240;

    this.drawBackgrounds(backgrounds, buffer.getContext('2d'), sprites);

    return function drawBackgroundLayer(context: CanvasRenderingContext2D) {
      context.drawImage(buffer, 0, 0);
    }
  }

  /**
   * Generates a layer for a Game Sprite
   * @param entity Entity children
   */
  public static createSpriteLayer(entity: Entity): Function {
    return function drawSpriteLayer(context: CanvasRenderingContext2D) {
      entity.draw(context);
    }
  }
}
