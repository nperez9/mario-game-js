/**
 * Manages all sprites layers on the game
 */
export class Compositor {
  private layers = [];

  public draw(context: CanvasRenderingContext2D) {   this.layers.forEach(layer =>  {
      layer(context);
    });
  }

  public addLayer(layer) {
    this.layers.push(layer);
  }
}