/**
 * Draws an image from a spriteSheet
 * @version 2
 */
export class SpriteSheet {
  private image: HTMLImageElement;
  private width: number;
  private height: number;
  private tiles: Map<string, any>;

  /** 
  * Initialize a entire Sprite Sheet
  * @param image The image of the spriteSheet
  * @param width Width of every sprite
  * @param heigth  Heigth of every sprite
  */
  constructor(image: any, width: number, height: number) {
    this.image = image;
    this.width = width;
    this.height = height;
    this.tiles = new Map();
  }

  /**
   * Define the a sprite in spriteSheet 
   * @param name Key of the in the spriteSheet tile
   * @param x Position x in the spriteSheet of the sprite
   * @param y Position y in the spriteSheet of the sprite
   * @param width Define the width in pixels of the sprites
   * @param height Define the height in pixels of the sprites
   */
  public define(name: string, x: number, y: number, width: number, height:number): void {
    const buffer = document.createElement('canvas');
    buffer.width = width;
    buffer.height = height;

    buffer.getContext('2d').drawImage(
      this.image,
      x,
      y,
      width,
      height,
      0,
      0,
      width,
      height
    );
    this.tiles.set(name, buffer); 
  }

  /**
  * Old define methed, define cool stuff for tiles
  * @param name Key of the in the spriteSheet tile
  * @param x Position x in the spriteSheet of the sprite
  * @param y Position y in the spriteSheet of the sprite
  */
  public defineTile(name: string, x: number, y: number) {
    this.define(name, x * this.width, y * this.height, this.width, this.height);
  }

  /**
   * Draws a sprite inside a 2dContext canvas, you must define it before use
   * @param name Key of the sprite
   * @param context canvas where gonna draw mutefucka
   * @param x Position X where this draw
   * @param y Position Y where this draw
   */
  public draw(name: string, context: CanvasRenderingContext2D, x: number, y: number): void {
    try {
      context.drawImage(this.tiles.get(name), x, y);
    } catch (e) {
      this.errorHandler(name, e);
    }
  }

  /**
   * Draws a sprite tile, (conseider the width and heigth of the sprite)
   * @param name Key of the sprite
   * @param context canvas where gonna draw mutefucka
   * @param x Position X where starts the tile
   * @param y Position Y where starts the tile
   */
  public drawTile(name: string, context: CanvasRenderingContext2D, x: number, y: number): void {
    this.draw(name, context, x  * this.width, y * this.height);
  }

  private errorHandler(name: string, e: Error) {
    console.error('SpriteSheet: ', name, 'Error:', e);
  }
}