import { ILevel } from 'src/interfaces';

export class Loader {
  static loadImage(url: string): Promise<any> {
    return new Promise(resolve => {
      const image = new Image();
      image.addEventListener('load', () => {
        resolve(image);
      });
      image.src = url;
    });
  }

  static loadLevel(level: string): Promise<ILevel> {
    return fetch(`/src/levels/${level}.json`)
      .then(r => r.json());
  }
} 