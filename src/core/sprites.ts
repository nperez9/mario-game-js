import { SpriteSheet, Loader } from '../gameUtils';

export function loadBackgroundSpritres(): Promise<SpriteSheet> {
  return Loader.loadImage('/public/assets/tileset.png').then(image => {
    const sprites = new SpriteSheet(image, 16, 16);
    sprites.defineTile('ground', 0, 0);
    sprites.defineTile('sky', 3, 23);
    return sprites;
  });
}

export function loadMarioSprite(): Promise<SpriteSheet> {
  return Loader.loadImage('/public/assets/characters.gif').then(image => {
    const sprites = new SpriteSheet(image, 16, 16);
    sprites.define('idle', 276, 43, 16, 16);
    return sprites;
  });
} 