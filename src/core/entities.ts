import { SpriteSheet } from 'src/gameUtils';
import { loadMarioSprite } from './sprites';
import { Mario } from './Mario';

export function createMario(): Promise<Mario> {
  return loadMarioSprite().then((marioSprite: SpriteSheet) => {
    const mario = new Mario(marioSprite);
    mario.position.set(64, 180);
    mario.velocity.set(200, -600);

    return mario;
  });
}