import { Entity, SpriteSheet } from '../gameUtils';

/**
 * A Mario class
 * needs an Spritesheet created for the entity
 */
export class Mario extends Entity {
  constructor(sprite: SpriteSheet) {
    super(sprite);
  }

  /**
   * The update method execs for every frame
   * velocity and position are entity data
   * @returns void
   */
  public update(deltaTime: number): void {
    this.position.y += this.velocity.y * deltaTime;
    this.position.x += this.velocity.x * deltaTime;
  }
}
