export interface ILevel {
  level: string,
  backgrounds: IBackground[]
}

export interface IBackground {
  tile: 'string',
  ranges: [number[]],
}