import { Loader, Compositor, Layers } from './gameUtils';
import { loadBackgroundSpritres } from './core/sprites';
import { createMario } from './core/entities';

const canvas = document.getElementById('screen') as HTMLCanvasElement;
const context = canvas.getContext('2d');

Promise.all([
  loadBackgroundSpritres(),
  Loader.loadLevel('1-1'), 
  createMario()
]).then(([
  backgroundSprite, 
  level,
  mario
]) => {
  const gravity = 0.5;

  const backgroundLayer = Layers.createBackground(level.backgrounds, backgroundSprite);
  const spriteLayer = Layers.createSpriteLayer(mario);
  const compositor = new Compositor();
  console.info(backgroundLayer);
  // compositor.addLayer(backgroundLayer);
  compositor.addLayer(spriteLayer);

  // time in miliseconds
  const deltaTime = 1/60;
  let accomulatedTime = 0;
  let lastTime = 0;

  const update = (time: number) => {
    accomulatedTime = (time - lastTime) / 1000;
    if (accomulatedTime > deltaTime) {
      compositor.draw(context);
      mario.update(deltaTime);
      mario.velocity.y += gravity;
    } 
    lastTime = time;
    requestAnimationFrame(update);
    setTimeout(update, 1000/60, performance.now());
  }

  update(0);
});